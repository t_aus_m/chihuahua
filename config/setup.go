package config

import (
	"codeberg.org/momar/chihuahua"
	"github.com/markbates/pkger"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"os"
	"path/filepath"
)

func Setup(sourcePath string, watch bool) *chihuahua.Config {
	f := sourcePath
	if f == "" {
		if info, err := os.Stat("./chihuahua.hcl"); err == nil && !info.IsDir() {
			f = "./chihuahua.hcl"
		} else if info, err := os.Stat(filepath.Join(os.Getenv("HOME"), ".config/chihuahua.hcl")); err == nil && !info.IsDir() {
			f = filepath.Join(os.Getenv("HOME"), ".config/chihuahua.hcl")
		} else if info, err := os.Stat("/etc/chihuahua.hcl"); err == nil && !info.IsDir() {
			f = "/etc/chihuahua.hcl"
		} else {
			// Write config
			configText := loadConfig()
			err := ioutil.WriteFile("/etc/chihuahua.hcl", configText, 0644)
			if err != nil {
				err = ioutil.WriteFile(filepath.Join(os.Getenv("HOME"), ".config/chihuahua.hcl"), configText, 0644)
				if err != nil {
					log.Error().Err(err).Msg("couldn't write default config to /etc/chihuahua.hcl or ~/.config/chihuahua.html")
					os.Exit(1)
				} else {
					f = filepath.Join(os.Getenv("HOME"), ".config/chihuahua.hcl")
				}
			} else {
				f = "/etc/chihuahua.hcl"
			}
			log.Info().Str("path", f).Msg("wrote default config")
		}
	} else if info, err := os.Stat(f); err != nil || info.IsDir() {
		configText := loadConfig()
		err = ioutil.WriteFile(f, configText, 0644)
		if err != nil {
			log.Error().Err(err).Str("path", f).Msg("couldn't write default config")
			os.Exit(1)
		} else {
			log.Info().Str("path", f).Msg("wrote default config")
		}
	}
	log.Info().Interface("path", f).Msg("selected configuration file")

	cfg := New()
	err := Parse(f, cfg)
	if err != nil {
		log.Error().Err(err).Msg("configuration parsing error")
		os.Exit(2)
	}
	log.Info().Msg("configuration has been loaded")

	if watch {
		log.Info().Err(err).Msg("watching configuration file changes...")
		go func() {
			err := Watch(f, cfg)
			if err != nil {
				log.Error().Err(err).Msg("configuration watcher error")
				os.Exit(2)
			} else {
				// This should never happen!!!
				log.Warn().Msg("configuration watcher exited")
			}
		}()
	}

	return cfg
}

func loadConfig() []byte {
	configFile, err := pkger.Open("codeberg.org/momar/chihuahua:/resources/chihuahua.hcl")
	if err != nil {
		log.Error().Err(err).Msg("couldn't open default config from web resources")
		os.Exit(1)
	}
	configText, err := ioutil.ReadAll(configFile)
	if err != nil {
		log.Error().Err(err).Msg("couldn't read default config from web resources")
		os.Exit(1)
	}
	return configText
}
