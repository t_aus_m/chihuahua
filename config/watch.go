package config

import (
	"codeberg.org/momar/chihuahua"
	"github.com/fsnotify/fsnotify"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

func Watch(sourcePath string, targetStruct *chihuahua.Config) error {
	for {
		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			return err
		}

		done := make(chan error)

		go func() {
			for {
				select {
				case _, ok := <-watcher.Events:
					if !ok {
						return
					}

					time.Sleep(100 * time.Millisecond)
					chihuahua.Working.Lock()

					// loop until the file is up-to-date (usually 1 time)
					var stat, err = os.Stat(sourcePath)
					var lastModification = time.Time{}
					for err == nil && lastModification != stat.ModTime() {
						lastModification = stat.ModTime()

						log.Trace().Msg("configuration change detected")
						err := Parse(sourcePath, targetStruct)
						if err != nil {
							log.Error().Err(err).Msg("configuration parsing error")
						} else {
							log.Info().Msg("configuration has been updated")
							chihuahua.Schedule(targetStruct)
						}

						stat, err = os.Stat(sourcePath)
					}
					chihuahua.Working.Unlock()
					if err != nil {
						log.Error().Err(err).Msg("configuration stat error (is the file accessible?)")
					}

					watcher.Close()
					done <- nil

				case err, ok := <-watcher.Errors:
					if !ok {
						return
					}
					watcher.Close()
					done <- err
				}
			}
		}()

		if err := watcher.Add(sourcePath); err != nil {
			watcher.Close()
			return err
		}
		if err := <-done; err != nil {
			watcher.Close()
			return err
		}
		watcher.Close()
	}
}
