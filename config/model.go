package config

import (
	"github.com/hashicorp/hcl/v2"
	"time"
)

type Config struct {
	Check []Check `hcl:"check,block"`
	Group []ServerOrGroup `hcl:"group,block"`
	Server []ServerOrGroup `hcl:"server,block"`
	Notifier []Notifier `hcl:"notifier,block"`

	RootURL *string `hcl:"root_url"`
	
	Disable *bool `hcl:"disable"`
	Notify *[]string `hcl:"notify"`
	Verify *uint `hcl:"verify"`
	Interval *string `hcl:"interval"`
	interval *time.Duration
}

type Context struct {
	Parent []string
	Disable bool
	Notify []string
	Verify uint
	Interval time.Duration
	Checks []Check
}

type Check struct {
	ID string `hcl:"id,label"`
	Name *string `hcl:"name"`
	Command string `hcl:"command"`

	Disable *bool `hcl:"disable"`
	Notify *[]string `hcl:"notify"`
	Verify *uint `hcl:"verify"`
	Interval *string `hcl:"interval"`
	interval *time.Duration
}

// Server and Group exist just to check if there are any invalid fields
type Empty struct {}

type ServerOrGroup struct {
	ID string `hcl:"id,label"`
	Name *string `hcl:"name"`
	Connection *string `hcl:"connection"`
	connectionType string

	Disable *bool `hcl:"disable"`
	Notify *[]string `hcl:"notify"`
	Verify *uint `hcl:"verify"`
	Interval *string `hcl:"interval"`
	interval *time.Duration

	Check []Check `hcl:"check,block"`
	Group []ServerOrGroup `hcl:"group,block"`
	Server []ServerOrGroup `hcl:"server,block"`

	Info hcl.Body `hcl:",remain"`
}

type Notifier struct{
	ID string `hcl:"id,label"`
	Type string `hcl:"type"`
	Remain hcl.Body `hcl:",remain"`
}
