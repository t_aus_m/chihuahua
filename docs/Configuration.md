# Configuration
Chihuahua is configured using the [HCL](https://github.com/hashicorp/hcl/tree/hcl2) configuration file format; the config file will be loaded from the `-c` command line option, or if it isn't specified from the first accessible file of those: `./chiuahua.hcl`, `~/.config/chihuahua.hcl` or `/etc/chihuahua.hcl`.

If no configuration file exists, Chihuahua will try to create the specified file, or one of `/etc/chihuahua.hcl` or `~/.config/chihuahua.hcl` if `-c` is not set.

An example configuration file can be found at [chihuahua.hcl](https://codeberg.org/momar/chihuahua/src/branch/master/resources/chihuahua.hcl).

## `root_url`
This top-level property should always be set to provide a link from notifications back to the status page.

## Configuration Blocks

A configuration block always looks somewhat like this (which would be an `example` block):

```hcl
example "an-example-id" {
  someProperty = "A value of the property"
}
```

IDs should not be using anything else than letters, numbers, dashes and underscores, as they're URL-encoded and will lead to confusing console output. You can use the `name` property of most blocks to assign a more human-readable name that'll be displayed on the status page.

### `check` block
A single check that will be run on its parent, which can be either a single [`server`](#server-block) or a [`group`](#group-block) of servers. It can also be specified at the top level, in which case all servers will run the check, unless it's being overwritten by a check with the same ID.

Checks can have the following properties:

#### `name`
This property sets the name displayed on the status page (defaults to the check's ID).

#### `command`
This **required** property defines the command to run as the check. It must work like a [Monitoring Plugin](https://www.monitoring-plugins.org/doc/guidelines.html) so Chihuahua can parse the output and exit code.

#### General Properties
Those properties can (additionally to checks) also be set directly on a [`server`](#server-block) or [`group`](#group-block) block (or at the top level), and affect all checks at that level and below in that case:

##### `disable`
If this is `true`, the check will not run and won't show up on the status page (defaults to `false`).

##### `notify`
An array of notifier IDs who should receive notifications for this check (defaults to all notifiers, or `null`).

##### `verify`
An unsigned integer that sets how many times a state change must be confirmed before a notification is being sent (defaults to `0`).
A `1` means that a state change has to be verified once, so it gets delayed by 1 check interval.

##### `interval`
How often the check should be executed, as a duration string [parsable by Go's time library](https://golang.org/pkg/time/#ParseDuration) (defaults to `"5m"`).

### `server` block
A single server with a single remote<!--, push--> or local connection.
It can occur at the top level or as a child of a [`group`](#group-block) block. It can also contain the [general properties](#general-properties) for checks, additionally to the following server properties:

#### `name`
This property sets the name displayed on the status page (defaults to the check's ID).

#### `connection`
This **required** property is used to define the remote server those checks should run on.

- `connection = "local"`
  Uses /bin/sh on the local machine to run checks. You can add a custom shell with e.g. `"local /bin/bash -c"` - the check command will just be appended as a single argument.
- `connection = "ssh xyz@example.org -p 2222"`  
  Uses an SSH connection with the specified arguments. Works just like the normal `ssh` command, so Chihuahua will also use the same private keys and configuration files.
<!--- `connection = "push your-push-token"`
  Use a [Chihuahua Push]() service to receive results passively. If no new result comes through after two intervals, the status will be "unknown".-->

### `group` block
A group of servers or other groups.
It can occcur at the top level or as a child of another `group` block. It can also contain the [general properties](#general-properties) for checks.

#### `name`
This property sets the name displayed on the status page (defaults to the check's ID).

### `notifier` block
A notification channel like "database-admins" or "clients", or "email" or "sms". Available properties depend on the value of the `type` property:

#### `type`
Currently supported types are:
- `type = "smtp"`
- `type = "gotify"`
- `type = "console"` (debugging only)

<!--
- `type = "sendmail"`
- `type = "clockworksms"`
- `type = "webhook"`
- `type = "command"`
- `type = "group"`
-->

Each type has different properties, which you can find listed below.

<!--
#### `sendmail` notifier
Uses a `sendmail`-compatible command to send email notifications.

Example:

```hcl
notifier "email" {
  type = "sendmail"
  to = "mail@example.org"
}
```

##### `from`
Set the sender address for emails (default is set by sendmail).

##### `to`
Array of recipients (required).

##### `binary`
Name/path of the `sendmail` binary (defaults to `sendmail`).
-->

#### `smtp` notifier
Uses an external SMTP server to send email notifications, or sends emails directly to the recipient's mailserver (the latter approach is unreliable though!).

Example:

```hcl
notifier "email" {
  type = "smtp"
  from = "noreply@example.org"
  to = ["mail@example.org"]
  server = "abcd:abcd@smtp.postmarkapp.com"
}
```

##### `from`
Set the sender address for emails (required).

##### `to`
Array of recipients (required).

##### `server`
Connection string of the SMTP server (defaults to an empty string, which means to send email directly to the recipient's server). Example:  `[username:password@]smtp.example.org[:25]`

The default port is `587`, and the connection is encrypted via StartTLS if possible.

##### `delay`
Emails are delayed by this value (default: `5m`, must be [parsable by Go's time library](https://golang.org/pkg/time/#ParseDuration)) to bundle multiple notifications together as a single email.

##### `crammd5`
Boolean value that you have to set to `true` if you want to use CRAM-MD5 instead of plain authentication. You probably don't need this.


<!--
#### `clockworksms` notifier
Send notifications via SMS using clockworksms.com.

Example:

```hcl
notifier "sms" {
  type = "clockworksms"
  to = "491517777777"
  token = "aaaaa..."
}
```

##### `to`
Array of recipient's phone numbers, according to https://www.clockworksms.com/doc/easy-stuff/http-interface/send-sms/#to (required).

##### `from`
The sender displayed on the phone, according to https://www.clockworksms.com/doc/easy-stuff/http-interface/send-sms/#from.

##### `token`
The API key from https://app3.clockworksms.com/Keys (required).
-->

#### `gotify` notifier
Use a [Gotify](https://gotify.net) server to deliver notifications.

Example:

```hcl
notifier "push" {
  type = "gotify"
  server = "https://gotify.example.org"
  token = "aaaaa..."
}
```

##### `server`
URL to the root endpoint of the Gotify serer to use (required).

##### `token`
The application token generated in the Gotify web interface (required).

<!--
#### `webhook` notifier
Calls a remote URL using a HTTP POST request with a JSON payload containing further information.

Example:

```hcl
notifier "custom" {
  type = "webhook"
  url = "https://example.org/webhook"
}
```

##### `url`
The URL to send the request to (required).

##### JSON Payload
TODO

#### `command` notifier
Executes a shell command with the notification details specified in environment variables.

Example:

```hcl
notifier "custom" {
  type = "command"
  command = "echo 'Hello World' && reboot"
}
```

##### `command`
The shell command to execute (required).

##### Environment variables
TODO

#### `group` notifier
A group notifier can be used to notify multiple people at once.

Example:

```hcl
notifier "example" {
  notifier "mail1" {
    type = "sendmail"
    to = "admin@example.org"
  }
  notifier "mail2" {
    type = "sendmail"
    to = "ceo@example.org"
  }
}
```

##### `notifier`
Object that contains other notifiers, which can be used in the `notify` property of e.g. checks by using `groupname/notifiername` (in this case, `example/mail2`). Using just the groupname implies all notifiers in that group (hence, `/` implies every notifier specified in the config).

-->