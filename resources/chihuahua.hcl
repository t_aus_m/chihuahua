# You can find the full configuration manual here:
# https://codeberg.org/momar/chihuahua/src/branch/master/docs/Configuration.md

interval = "15m"
notify = ["email"]

check "cpu" {
  # This check will be run on every server, unless it specifies an overriding check called "cpu"
  name = "CPU Load"
  verify = 2
  interval = "5m"
  command = "check_load -r -w 3,2,1.25 -c 4,3,2"
}

server "local" {
  name = "Local Tests"
  connection = "local"
  check "example.org" {
    command = "check_http -w 5 -c 10 --ssl -H example.org"
  }
  check "example.com" {
    command = "check_http -w 5 -c 10 --ssl -H example.com"
  }
}

group "example" {
  name = "Example Group"

  check "disk" {
    name = "Disk Space"
    command = "check_sudo check_disk -w 15% -c 5%"
    interval = "6h"
    notify = ["email", "important"]
  }

  server "example.org" {
    name = "Example Server (.org)"
    connection = "ssh chihuahua@example.org -p 2222"

    check "ram" {
      command = "check_memory -w 8 -c 3"
    }
  }

  server "example.com" {
    name = "Example Server (.com)"
    connection = "ssh chihuahua@example.com -p 2222"
  }
}

notifier "email" {
  # emails are delayed by 5 minutes by default to accumulate
  # multiple notifications into a single notification email.
  type = "smtp"
  # server = "user:password@smtp.example.org"
  from = "Chihuahua <chihuahua@example.org>"
  to = ["mail@example.org"]
}

notifier "important" {
  type = "gotify"
  server = "https://gotify.example.org"
  token = "abcdefg..."
}