// Leftpad - P(value, length, character, rightpad) - https://gist.github.com/moqmar/9ef4f0755d0a17b61458f35b91447bc1
function P(v,l,c,r) { v = v.toString(); while (v.length < l) v = r ? v + (c||0) : (c||0) + v; return v; }

var macies = [];
function updateMacy() {
    [...document.getElementsByClassName("masonry")].forEach(x => {
        if (!x.getAttribute("macy-id")) {
            x.setAttribute("macy-id", macies.length);
            macies.push(Macy({
                container: x,
                trueOrder: false,
                waitForImages: false,
                margin: 24,
                columns: 4,
                breakAt: { 940: 3, 520: 2, 400: 1 }
            }));
        } else macies[x.getAttribute("macy-id")].reInit();
    });
}

window.app = new Vue({
    el: "#app",
    data: {
        servers: [],
    },
    methods: {
        headerByStatus(status) {
            switch (status) {
                case 0: return ["has-background-success", "has-text-white"];
                case 1: return ["has-background-warning"];
                case 2: return ["has-background-danger", "has-text-white"];
                default: return ["has-background-black", "has-text-warning"];
            }
        },
        iconByStatus(status) {
            switch (status) {
                case 0: return "icons/checkmark-outline.svg";
                case 1: return "icons/alert-triangle-outline.svg";
                case 2: return "icons/close-outline.svg";
                default: return "icons/question-mark-outline.svg";
            }
        },
        dateFormat(dateString) {
            let date = new Date(dateString);
            return date.getFullYear() + "-" + P(date.getMonth() + 1, 2) + "-" + P(date.getDate(), 2) + " " + P(date.getHours(), 2) + ":" + P(date.getMinutes(), 2) + ":" + P(date.getSeconds(), 2);
        }
    },
});

function update() {
    fetch("checks").then(r => r.json()).then(r => {
        for (let i = 0; i < r.length; i++) {
            // we don't support groups yet :(
            if (r[i].Children) {
                for (let j = 0; j < r[i].Children.length; j++) r[i].Children[j].ID = r[i].ID + "/" + r[i].Children[j].ID;
                r.splice(i, 1, ...r[i].Children);
                i--;
            }
        }
        Vue.set(app, "servers", r);
        setTimeout(updateMacy);
        setTimeout(update, 1000);
    }).catch(err => {
        console.error(err);
        setTimeout(update, 250);
    })
}
update();
