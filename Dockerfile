FROM golang:1-alpine AS build

RUN apk --no-cache add git
COPY ./go.mod /build/go.mod
WORKDIR /build
RUN go mod download

# Using v0.12.8 because of https://github.com/markbates/pkger/issues/56
RUN go get github.com/markbates/pkger@v0.12.8 && go build -o /bin/pkger github.com/markbates/pkger/cmd/pkger

COPY . /build
RUN go generate
RUN go build -ldflags '-s -w' -o chihuahua ./cmd


FROM alpine

RUN apk add --no-cache openssh ca-certificates monitoring-plugins
RUN mkdir -p /data/.ssh &&\
    echo -e "Host *\n  StrictHostKeyChecking accept-new\n  UpdateHostKeys yes\n  IdentityFile /data/.ssh/id_rsa\n  IdentityFile /data/.ssh/id_dsa\n  IdentityFile /data/.ssh/id_ecdsa\n  IdentityFile /data/.ssh/id_ed25519\n  UserKnownHostsFile /data/.ssh/known_hosts" >> /etc/ssh/ssh_config

COPY --from=build /build/chihuahua /bin/chihuahua
EXPOSE 80
ENV PORT 80
ENV PATH /usr/lib/monitoring-plugins:/usr/local/bin:/usr/bin:/bin
ENV HOME /data
WORKDIR /data
ENTRYPOINT ["/bin/chihuahua", "-c", "/data/chihuahua.hcl"]
