# chihuahua
**The smallest watchdog on earth.** Tiny, [monitoring-plugins](https://www.monitoring-plugins.org/) compatible monitoring with a status page, built with [Go](https://golang.org).

- Run nagios/monitoring-plugins checks on a remote server
- Alert on state change via [Gotify](https://gotify.net) or email
- Show a nice little status page

![Screenshot](https://i.vgy.me/WKL8Dl.png)

## Getting Started
```bash
docker run -d -p 8080:80 -v "$PWD/data:/data" momar/chihuahua
nano data/chihuahua.hcl
```

Chihuahua now runs at http://localhost:8080.

You can find a full example configuration file at [chihuahua.hcl](https://codeberg.org/momar/chihuahua/src/branch/master/resources/chihuahua.hcl), and should also read the [full configuration manual](https://codeberg.org/momar/chihuahua/src/branch/master/docs/Configuration.md), but to get started, you can use a simple configuration like this:

```hcl
check "cpu" {
  # This check will be run on every server, unless the 
  # server specifies an overriding check called "cpu".
  name = "CPU Load"
  command = "check_load -r -w 3,2,1.25 -c 4,3,2"
}

server "server-local" {
  connection = "local"
}

server "server-01" {
  connection = "ssh chihuahua@example.org"
  check "ram" {
    command = "check_memory -w 8 -c 3" # parameters in percent
  }
}

notifier "email-myself" {
  # emails are delayed by 5 minutes by default to accumulate
  # multiple notifications into a single notification email.
  type = "smtp"
  server = "user:password@smtp.example.org"
  from = "Chihuahua <chihuahua@example.org>"
  to = ["myself@example.org"]
}
```

<details>
<summary>Use a systemd service to install the server without Docker (unsupported)</summary>

```bash
wget https://codeberg.org/momar/chihuahua/releases/download/v1.4/chihuahua-x64.gz -O- | gunzip > /tmp/chihuahua
sudo install -m755 /tmp/chihuahua /usr/local/bin/chihuahua

mkdir -p /usr/local/lib/chihuahua/.ssh
useradd -d /usr/local/lib/chihuahua -M -r -s /usr/sbin/nologin chihuahua
ssh-keygen -b 2048 -f /usr/local/lib/chihuahua/.ssh/id_rsa -P "" -C "Chihuahua Monitoring"
nano /etc/chihuahua.yml

sudo wget https://codeberg.org/momar/chihuahua/raw/branch/master/chihuahua.service -O /etc/systemd/system/chihuahua.service
sudo systemctl enable chihuahua.service
sudo systemctl start chihuahua.service
```
</details>

### Set up a server for connections (Debian/Ubuntu/Alpine/...)
```bash
curl -Ls http://status.example.org:8080/setup.sh | sudo sh
```

You can now use `connection = "ssh chihuahua@example.org"` to connect to your server with a limited user.

To completely **remove** the Chihuahua setup from your server, use the following commands:

```bash
sudo userdel -r chihuahua
sudo apt-get remove --auto-remove monitoring-plugins # or "apk del monitoring-plugins" on Alpine
sudo rm /usr/local/bin/check_sudo
sudo sed -i '/^chihuahua /d' /etc/sudoers
```

## API
TODO: this should be documented more thoroughly - maybe provide an API Blueprint?
```
GET /setup.sh
GET /checks
GET /checks/:server
GET /checks/:server/:check
```

## Development
Requires [Go](https://golang.org)
```bash
git clone https://codeberg.org/momar/chihuahua.git && cd chihuahua
cp resources/chihuahua.hcl .
go run ./cmd --debug
```

## Roadmap
- Add custom messages to checks
- Add private/hidden checks
- More notification providers (mainly [Clockwork SMS](http://clockworksms.com/))
- Gitea integration
- Provide a Prometheus exporter
