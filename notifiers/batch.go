package notifiers

import (
	"codeberg.org/momar/chihuahua"
	"sync"
	"time"
)

type BatchQueue struct {
	Execute BatchSender
	Timeout time.Duration
	working sync.Mutex
	queue   []BatchNotification
}

type BatchNotification struct {
	chihuahua.Check
	FullID         []string
	FullName       []string
	PreviousResult chihuahua.CheckResult
}

type BatchPrefix struct {
	ID   string
	Name string
}

type BatchSender func(*chihuahua.Config, []BatchNotification, []BatchPrefix)

func (q *BatchQueue) Enqueue(cfg *chihuahua.Config, check chihuahua.Check, previous chihuahua.CheckResult) {
	q.working.Lock()
	if q.queue == nil {
		q.queue = []BatchNotification{{check, check.FullID(), check.FullName(), previous}}
		go func() {
			// TODO: also run when exiting the application (or before something else bad happens)
			time.Sleep(q.Timeout)
			q.working.Lock()
			if len(q.queue) < 1 {
				return
			}

			var commonPrefix = []BatchPrefix{}
			id := q.queue[0].FullID
			name := q.queue[0].FullName
			for i := 0; i < len(id); i++ {
				isCommon := true
				for _, notification := range q.queue {
					if notification.FullID[i] != id[i] {
						isCommon = false
						break
					}
				}
				if isCommon {
					commonPrefix = append(commonPrefix, BatchPrefix{id[i], name[i]})
				} else {
					break
				}
			}

			q.Execute(cfg, q.queue, commonPrefix)
			q.queue = nil
			q.working.Unlock()
		}()
	} else {
		q.queue = append(q.queue, BatchNotification{check, check.FullID(), check.FullName(), previous})
	}
	q.working.Unlock()
}
