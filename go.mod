module codeberg.org/momar/chihuahua

go 1.12

require (
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82
	github.com/flosch/pongo2 v0.0.0-20190707114632-bbf5a6c351f4
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/hashicorp/hcl/v2 v2.2.0
	github.com/markbates/pkger v0.14.0
	github.com/matcornic/hermes/v2 v2.0.2
	github.com/rs/zerolog v1.18.0
	github.com/teris-io/cli v1.0.1
	golang.org/x/crypto v0.0.0-20200214034016-1d94cc7ab1c6 // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
	golang.org/x/sys v0.0.0-20200212091648-12a6c2dcc1e4 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
